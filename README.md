# Final Project

# Kelompok 15

# Anggota Kelompok
- <h2>Junizar Fajri</h2>
- <h2>Rahmat Januardi</h2>
- <h2>Naura Fathiahaq Nabila</h2><br>

# Tema Project
<h2>Portal Berita</h2><br>

# ERD
<p align="center">
<img src="https://i.ibb.co/HtCj8J1/kelompok15.png" width="400">
</p><br>

# Link Video
<h2>Link Video Demo Aplikasi :
    <a href="https://www.youtube.com/watch?v=iHQPX4bbDbM&ab_channel=RahmatJanuardi">https://www.youtube.com/watch?v=iHQPX4bbDbM&ab_channel=RahmatJanuardi</a>
</h2>
<h2>Link Website : 
    <a href="http://portal-berita-pks.herokuapp.com/">portal-berita-pks.herokuapp.com</a>
</h2>
