@extends('template.user')
@section('isi')

    <div class="about-area">
        <div class="container">
            <!-- Hot Aimated News Tittle-->
            <div class="row">
                <div class="col-lg-8">
                    <!-- Trending Tittle -->
                    <div class="about-right mb-30">
                        <div class="about-img">
                            <img style="width: 600px;height: 300px" src="{{ asset('foto/' . $news->foto) }}" alt="">
                        </div>
                        <div class="section-tittle mb-30 pt-30">
                            <h3>{{ $news->judul }}</h3>
                            <h5>Penulis : <b>{{ $news->author }}</b>, {{ date('d-m-Y', strtotime($news->tanggal)) }}
                            </h5>
                        </div>
                        <div class="about-prea">
                            <p>
                                {{ wordwrap($news->isi, 70) }}
                            </p>
                        </div>
                        <div class="social-share pt-30">
                            <h3 class="mr-20">Komentar:</h3>
                            @if (Session::has('success'))
                                <div class="alert alert-info">
                                    <p>{{ Session::get('success') }}</p>
                                </div>
                            @endif
                            <br>
                            @foreach ($komen as $k)
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <p><b>{{ $k->nama }}</b> | {{ date('d-m-Y', strtotime($k->tanggal)) }}</p>
                                        <p>{{ $k->keterangan }}</p>
                                        <hr>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- From -->
                    <div class="row">
                        <div class="col-lg-8">
                            <form class="form-contact contact_form mb-20"
                                action="{{ route('user.komentar_news', $news->id) }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea class="form-control w-100 error" name="isi" id="isi" cols="30"
                                                rows="9" onfocus="this.placeholder = ''"
                                                onblur="this.placeholder = 'Enter Message'"
                                                placeholder="Enter Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input class="form-control error" name="nama" id="nama" type="text"
                                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Masukkan Nama'"
                                                placeholder="Masukkan Nama">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input class="form-control error" name="email" id="email" type="text"
                                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Masukkan Email'"
                                                placeholder="Masukkan Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-3">
                                    <button type="submit" class="button button-contactForm boxed-btn">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <!-- Section Tittle -->
                    <div class="section-tittle mb-40">
                        <h3>Tranding News</h3>
                    </div>
                    <!-- Flow Socail -->
                    @forelse ($semua as $tp)
                        <div class="trand-right-single d-flex mb-30">
                            <div class="trand-right-img mr-3">
                                <img style="width: 60px;height:60px;" src="{{ asset('foto/' . $tp->foto) }}" alt="">
                            </div>
                            <div class="trand-right-cap">
                                <span class="color1 px-2 py-2 mb-4">{{ $tp->kategori->nama }}</span>
                                <h4 style="margin-top: 8px"><a
                                        href="{{ route('user.show_news', $tp->id) }}">{{ Str::limit($tp->judul, 30) }}</a>
                                </h4>
                            </div>
                        </div>
                    @empty
                        <h4>Data Berita Belum Ada</h4>
                    @endforelse
                    <!-- New Poster -->
                    <div class="news-poster d-none d-lg-block mt-40">
                        <img src="{{ asset('newMaster/assets/img/news/news_card.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
