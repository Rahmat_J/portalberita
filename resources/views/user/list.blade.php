@extends('template.user')
@section('isi')

    <div class="container">
        <div class="row mb-40">
            <div class="col-lg-8 mb-5 mb-lg-0 mt-40">
                <div class="blog_left_sidebar">
                    <article class="blog_item">
                        @forelse ($ekonomi as $e)
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="{{ asset('foto/' . $e->foto) }}" alt="">
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="{{route('user.show_news',$e->id)}}">
                                    <h2>{{ $e->judul }}</h2>
                                </a>
                                <p>{{ Str::limit($e->isi, 150) }}</p>
                                <ul class="blog-info-link">
                                    <li><a href="#"><i class="fa fa-user"></i>{{ $e->author }}</a></li>
                                    <li><a href="#">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            {{ date('d-m-Y', strtotime($e->tanggal)) }}
                                        </a>
                                    </li>
                                </ul>
                                </ul>
                            </div>
                        @empty
                            <h4>Data Berita Belum Ada</h4>
                        @endforelse
                    </article>
                </div>
            </div>
            <div class="col-lg-4 mt-40">
                <div class="blog_right_sidebar">
                    <div class="section-tittle mb-40">
                        <h3>Tranding News</h3>
                    </div>
                    <!-- Flow Socail -->
                    @forelse ($semua as $tp)
                        <div class="trand-right-single d-flex mb-30">
                            <div class="trand-right-img mr-3">
                                <img style="width: 60px;height:60px;" src="{{ asset('foto/' . $tp->foto) }}" alt="">
                            </div>
                            <div class="trand-right-cap">
                                <span class="color1 px-2 py-2 mb-4">{{ $tp->kategori->nama }}</span>
                                <h4 style="margin-top: 8px"><a
                                        href="{{ route('user.show_news', $tp->id) }}">{{ Str::limit($tp->judul, 30) }}</a>
                                </h4>
                            </div>
                        </div>
                    @empty
                        <h4>Data Berita Belum Ada</h4>
                    @endforelse
                    <!-- New Poster -->
                    <div class="news-poster d-none d-lg-block mt-40">
                        <img src="{{ asset('newMaster/assets/img/news/news_card.jpg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
