@extends('template.user')
@section('isi')

    <div class="trending-area fix">
        <div class="container">
            <div class="trending-main">
                <!-- Trending Tittle -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="trending-tittle">
                            <strong>Trending now</strong>
                            <!-- <p>Rem ipsum dolor sit amet, consectetur adipisicing elit.</p> -->
                            <div class="trending-animated">
                                <ul id="js-news" class="js-hidden">
                                    <li class="news-item">Berita Terbaru dari Kelompok 15.</li>
                                    <li class="news-item">Project Mengenai Tema Portal Berita.</li>
                                    <li class="news-item">Selamat Menikmat Sajian Informasi terupdate</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class=" bg-info" style="border-radius: 5px; text-align:center; color:white">
                            <div class="section-tittle mt-5">
                                <h3 style="color:white">Top News</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <!-- Trending Top -->
                        @forelse ($semua as $s)
                            <div class="trending-top">
                                <div class="trend-top-img mb-50">
                                    <img style="width: 400px;height: 250px" src="{{ asset('foto/' . $s->foto) }}" alt="">
                                    <div class="trend-top-cap">
                                        <span>{{ $s->kategori->nama }}</span>
                                        <h2><a
                                                href="{{ route('user.show_news', $s->id) }}">{{ Str::limit($s->judul, 35) }}</a>
                                        </h2>
                                    </div>
                                </div>
                            </div>

                        @empty
                            <h4>Data Berita yang Trending Belum Ada</h4>
                        @endforelse
                        <!-- Trending Bottom -->
                    </div>
                    <!-- Riht content -->
                    <div class="col-lg-4">
                        @forelse ($tops as $tp)
                            <div class="trand-right-single d-flex">
                                <div class="trand-right-img">
                                    <img style="width: 100px;height:80px;"  src="{{ asset('foto/' . $tp->foto) }}" alt="">
                                </div>
                                <div class="trand-right-cap">
                                    <span class="color1">{{ $tp->kategori->nama }}</span>
                                    <h4><a
                                            href="{{ route('user.show_news', $tp->id) }}">{{ Str::limit($tp->judul, 30) }}</a>
                                    </h4>
                                </div>
                            </div>
                        @empty
                            <h4>Data Berita Belum Ada</h4>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="recent-articles">
        <div class="container">
            <div class="recent-wrapper">
                <!-- section Tittle -->
                <div class="row pb-3">
                    <div class="col-lg-12">
                        <div class="section-tittle mb-30">
                            <h3>Berita Lainnya</h3>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            @foreach ($ekonomi as $e)
                                <div class="col-sm-3 mb-4">
                                    <img style="width: 250px;height: 200px" class="img-rounded img-responsive"
                                        src="{{ asset('foto/' . $e->foto) }}" alt="">
                                </div>
                                <div class="col-sm-9">
                                    <h2 class="bg-info px-2 py-2"
                                        style="width: 100px; border-radius: 5px; text-align:center; color:white">
                                        {{ $e->kategori->nama }}</h2>
                                    <a class="d-inline-block" href="single-blog.html">
                                        <h2>{{ $e->judul }}</h2>
                                    </a>
                                    <p>{{ Str::limit($e->isi, 30) }}</p>
                                    <ul class="blog-info-link">
                                        <li><a href="#"><i class="fa fa-user"></i>{{ $e->author }}</a></li>
                                        <li><a href="#">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                {{ date('d-m-Y', strtotime($e->tanggal)) }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach
                            @foreach ($olahraga as $o)
                                <div class="col-sm-3 mb-4">
                                    <img style="width: 250px;height: 200px" class="img-rounded img-responsive"
                                        src="{{ asset('foto/' . $o->foto) }}" alt="">
                                </div>
                                <div class="col-sm-9">
                                    <h2 class="bg-info px-2 py-2"
                                        style="width: 100px; border-radius: 5px; text-align:center; color:white">
                                        {{ $o->kategori->nama }}</h2>
                                    <a class="d-inline-block" href="single-blog.html">
                                        <h2>{{ $o->judul }}</h2>
                                    </a>
                                    <p>{{ Str::limit($o->isi, 30) }}</p>
                                    <ul class="blog-info-link">
                                        <li><a href="#"><i class="fa fa-user"></i>{{ $o->author }},
                                                {{ $o->tanggal }}</a></li>
                                        <li><a href="#">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                {{ date('d-m-Y', strtotime($o->tanggal)) }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach
                            @foreach ($politik as $p)
                                <div class="col-sm-3 mb-4">
                                    <img style="width: 250px;height: 200px" class="img-rounded img-responsive"
                                        src="{{ asset('foto/' . $p->foto) }}" alt="">
                                </div>
                                <div class="col-sm-9">
                                    <h2 class="bg-info px-2 py-2"
                                        style="width: 100px; border-radius: 5px; text-align:center; color:white">
                                        {{ $p->kategori->nama }}</h2>
                                    <a class="d-inline-block" href="single-blog.html">
                                        <h2>{{ $p->judul }}</h2>
                                    </a>
                                    <p>{{ Str::limit($p->isi, 30) }}</p>
                                    <ul class="blog-info-link">
                                        <li><a href="#"><i class="fa fa-user"></i>{{ $p->author }},
                                                {{ $p->tanggal }}</a></li>
                                        <li><a href="#">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                {{ date('d-m-Y', strtotime($p->tanggal)) }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach
                            @foreach ($tekno as $tek)
                                <div class="col-sm-3 mb-4">
                                    <img style="width: 250px;height: 200px" class="img-rounded img-responsive"
                                        src="{{ asset('foto/' . $tek->foto) }}" alt="">
                                </div>
                                <div class="col-sm-9">
                                    <h2 class="bg-info px-2 py-2"
                                        style="width: 100px; border-radius: 5px; text-align:center; color:white">
                                        {{ $tek->kategori->nama }}</h2>
                                    <a class="d-inline-block" href="single-blog.html">
                                        <h2>{{ $p->judul }}</h2>
                                    </a>
                                    <p>{{ Str::limit($tek->isi, 30) }}</p>
                                    <ul class="blog-info-link">
                                        <li><a href="#"><i class="fa fa-user"></i>{{ $p->author }},
                                                {{ $tek->tanggal }}</a></li>
                                        <li><a href="#">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                {{ date('d-m-Y', strtotime($tek->tanggal)) }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
