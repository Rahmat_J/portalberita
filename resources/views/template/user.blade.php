<!DOCTYPE html>
<html>

<head>
	<title>Kelompok 15</title>
	<link href=" {{asset('css/bootstrap.min.css')}}" rel="stylesheet" media="all">

	<!-- CSS here -->
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/ticker-style.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/flaticon.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/slicknav.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/animate.min.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/magnific-popup.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/fontawesome-all.min.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/themify-icons.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/slick.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/nice-select.css')}}">
	<link rel="stylesheet" href="{{asset('newMaster/assets/css/style.css')}}">
</head>

<body>
	@include('partial.navUser')

	<div class="container">
		@yield('isi')
	</div>

	@include('partial.footerUser')
	<script src="{{asset('vendor/jquery-3.2.1.min.js')}} "></script>
	<script src="{{asset('vendor/bootstrap-4.1/popper.min.js')}} "></script>
	<script src="{{asset('vendor/bootstrap-4.1/bootstrap.min.js')}} "></script>

	<!-- All JS Custom Plugins Link Here here -->
	<script src="{{asset('newMaster/assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="{{asset('newMaster/assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/popper.min.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/bootstrap.min.js')}}"></script>
	<!-- Jquery Mobile Menu -->
	<script src="{{asset('newMaster/assets/js/jquery.slicknav.min.js')}}"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="{{asset('newMaster/assets/js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/slick.min.js')}}"></script>
	<!-- Date Picker -->
	<script src="{{asset('newMaster/assets/js/gijgo.min.js')}}"></script>
	<!-- One Page, Animated-HeadLin -->
	<script src="{{asset('newMaster/assets/js/wow.min.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/animated.headline.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/jquery.magnific-popup.js')}}"></script>

	<!-- Breaking New Pluging -->
	<script src="{{asset('newMaster/assets/js/jquery.ticker.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/site.js')}}"></script>

	<!-- Scrollup, nice-select, sticky -->
	<script src="{{asset('newMaster/assets/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/jquery.nice-select.min.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/jquery.sticky.js')}}"></script>
	
	<!-- contact js -->
	<script src="{{asset('newMaster/assets/js/contact.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/jquery.form.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/mail-script.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/jquery.ajaxchimp.min.js')}}"></script>
	
	<!-- Jquery Plugins, main Jquery -->	
	<script src="{{asset('newMaster/assets/js/plugins.js')}}"></script>
	<script src="{{asset('newMaster/assets/js/main.js')}}"></script>
	@stack('script')
</body>

</html>