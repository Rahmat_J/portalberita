<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarModel extends Model
{
    protected $table = "komentar";
    public function berita()
    {
        return $this->belongsTo('App\BeritaModel');
    }
}
